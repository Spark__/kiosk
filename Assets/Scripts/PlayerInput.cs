﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SillhouetteGames
{
    public class PlayerInput : MonoBehaviour
    {
        public float Vertical { get; private set; }
        public float Horizontal { get; private set; }
        private bool Fire { get; set; }

        public event Action OnFire = delegate { };
        private void Update()
        {
            Horizontal = Input.GetAxis("Mouse X");
            Vertical = Input.GetAxis("Vertical");
            Fire = Input.GetButtonDown("Fire1");
            if (Fire)
                OnFire();
        }
    }
}