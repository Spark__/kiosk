﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets;

namespace SillhouetteGames 
{
    [RequireComponent(typeof(PlayerInput))]
    public class CarMotor : MonoBehaviour
    {
        //protected CarController carController;
        private PlayerInput playerInput;
        private void Awake()
        {
            //if (!photonView.IsMine)
            //    Destroy(GetComponent<PlayerInput>());

            //characterAnimator = GetComponent<Animator>();
            playerInput = GetComponent<PlayerInput>();
        }

        private void Update()
        {
            transform.position += new Vector3(playerInput.Horizontal, 0, playerInput.Vertical);

        }
    }
}
