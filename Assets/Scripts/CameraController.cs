﻿using Cinemachine;
using Photon.Pun;
using UnityEngine;

namespace SillhouetteGames
{
    public class CameraController : MonoBehaviourPun
    {
        private CinemachineComposer composer;

        [SerializeField]
        private float sensitivity = 1f;
        private Transform headBone;
        private PhotonView PhotonView;

        private void Awake()
        {
            PhotonView = GetComponent<PhotonView>();

            if (!PhotonView.IsMine)
                Destroy(GetComponentInChildren<CinemachineVirtualCamera>());
        }

        private void Start()
        {
            composer = GetComponentInChildren<CinemachineVirtualCamera>().GetCinemachineComponent<CinemachineComposer>();
            //PlayerMotor[] scenePlayers = FindObjectsOfType<PlayerMotor>();
            //foreach (var scenePlayer in scenePlayers)
            {
                //  if (scenePlayer.photonView.IsMine)
                if (photonView.IsMine)
                {
                    headBone = GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head);
                    GetComponent<CinemachineVirtualCamera>().m_Follow = transform;
                    GetComponent<CinemachineVirtualCamera>().m_LookAt = headBone;
                }
            }

        }

        private void lateUpdate()
        {
            float vertical = Input.GetAxis("Mouse Y") * sensitivity;
            composer.m_TrackedObjectOffset.y += vertical;
            composer.m_TrackedObjectOffset.y = Mathf.Clamp(composer.m_TrackedObjectOffset.y, -10, 10);
        }
    }
}
