﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace SillhouetteGames
{
    //[RequireComponent(typeof(NetworkConnectionInfo))]
    public class NetworkConnectionManager : MonoBehaviourPunCallbacks
    {
        [Header("Drag Comonents From UI to this section")]
        [SerializeField] TMP_InputField UI_NickName;
        [SerializeField] TMP_InputField UI_RoomName;
        [SerializeField] Button UI_ConnectToMaster;
        [SerializeField] TMP_Text UI_ConnectionStatus;
        [SerializeField] int UI_GameSceneIndex;
        [SerializeField] int UI_MaxPlayersPerRoom;
        [SerializeField] Toggle UI_Toggle;
        [SerializeField] GameObject PlayerPrefab;

        public string NickName { get; private set; }
        public string RoomName { get; private set; }
        public string GameVersion { get; private set; }
        public string PlayerPrefabName { get; private set; }

        private bool offlineMode;
        private bool hideRoom;
        private int maxPlayersPerRoom;
        protected bool isConnectingToMaster;
        private RoomOptions roomOptions;
        protected bool isJoiningRoom;
        private int gameSceneIndex;

        public static NetworkConnectionManager instance;
        private void Awake()
        {
            if (instance != null)
                Destroy(gameObject);
            else
                instance = this;

            DontDestroyOnLoad(gameObject);
            InitializeConnectionSettings();
        }

        TextInfo UsaTextInfo = new CultureInfo(1, false).TextInfo;
        private void InitializeConnectionSettings()
        {
            this.NickName = UsaTextInfo.ToTitleCase(System.Environment.UserName.Replace(".", " "));
            this.RoomName = "Default";//NickName + "_Room" + Random.Range(111, 9999);
            this.offlineMode = UI_Toggle.isOn;
            this.GameVersion = Application.version;
            this.gameSceneIndex = UI_GameSceneIndex;
            this.hideRoom = false;
            this.maxPlayersPerRoom = 10;

        }

        public void TglOfflineMode_OnValueChanged(bool isOn)
        {
            UI_RoomName.transform.parent.parent.gameObject.SetActive(isOn);
        }

        private IEnumerator SetupInterfaceComponents()
        {
            yield return new WaitForSeconds(0.1f); //delay for username to load and tmpro to render, also check EventSystem gameobject as sibilling on the same level
            UI_NickName.text = this.NickName;
            UI_RoomName.text = this.RoomName;
            UI_MaxPlayersPerRoom = this.maxPlayersPerRoom;

            UI_ConnectToMaster.enabled = true;
            UI_ConnectToMaster.GetComponent<Button>().onClick.AddListener(() => this.btnConnectToMaster_OnClick());
            UI_Toggle.GetComponent<Toggle>().onValueChanged.AddListener((bool value) => TglOfflineMode_OnValueChanged(UI_Toggle.isOn));
        }


        private void Start()
        {
            StartCoroutine(SetupInterfaceComponents());

        }



        private void OnValidate()
        {
            UI_ConnectToMaster?.transform.SetAsLastSibling();
        }

        private void Update()
        {
            if (UI_ConnectToMaster != null)
                UI_ConnectToMaster.enabled = (!PhotonNetwork.IsConnected && !isConnectingToMaster);
        }

        private void SetupConnectionSettings(string _nickName, string _roomName, bool _offlineMode, string _gameVersion)
        {
            this.NickName = _nickName;
            this.RoomName = _roomName;
            this.offlineMode = _offlineMode;
            this.GameVersion = _gameVersion;
        }

        public void btnConnectToMaster_OnClick()
        {
            //might be a good place to setup player character and level
            PlayerPrefabName = PlayerPrefab.name;
            //Debug.Log("Connect btn clicked");
            this.isConnectingToMaster = true;
            this.NickName = UI_NickName.text;
            this.RoomName = UI_RoomName.text;
            SetupConnectionSettings(this.NickName, this.RoomName, this.offlineMode, this.GameVersion);
            //Debug.Log(this.NickName + this.RoomName + this.offlineMode + this.GameVersion);
            PhotonNetwork.ConnectUsingSettings();
        }

        public override void OnConnectedToMaster()
        {
            base.OnConnectedToMaster();
            //Debug.Log("Connected to Master");
            isConnectingToMaster = false;
            roomOptions = new RoomOptions() { IsVisible = this.hideRoom, MaxPlayers = (byte)maxPlayersPerRoom };
            //PlayerPrefabName = PlayerPrefab.name;
            UI_ConnectionStatus.text = "Connected to Master";

            if (offlineMode)
                SceneManager.LoadScene(UI_GameSceneIndex);
            else
                btnJoinRoom_OnClick();
        }

        public override void OnDisconnected(DisconnectCause cause)
        {
            //one reason is timeout due to firewall
            base.OnDisconnected(cause);
            //Debug.Log("Disconnected: " + cause); 
            isConnectingToMaster = false;
            isJoiningRoom = false;
            UI_ConnectionStatus.text = "Disconnected";
        }

        public void btnJoinRoom_OnClick()
        {

            //Debug.Log("btn JoinRoom");
            if (PhotonNetwork.IsConnected)
            {
                this.isJoiningRoom = true;
                PhotonNetwork.JoinOrCreateRoom(this.RoomName, roomOptions, TypedLobby.Default);
            }
        }

        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();
            //Debug.Log("onJoinedRoom: "+ PhotonNetwork.CurrentRoom.Name);
            PhotonNetwork.NickName = this.NickName;
            isJoiningRoom = false;
            UI_NickName.gameObject.transform.parent.parent.gameObject.SetActive(false);
            UI_ConnectionStatus.text = "Joined Room: " +
                PhotonNetwork.CurrentRoom.Name +
                (PhotonNetwork.IsMasterClient ? " as Master | " : " with ") +
                (PhotonNetwork.CurrentRoom.PlayerCount - 1) + " Other Player" + ((PhotonNetwork.CurrentRoom.PlayerCount > 1) ? "s" : "" + " Found");

            SceneManager.LoadScene(this.gameSceneIndex);//loading next scene       


        }


        public override void OnCreateRoomFailed(short returnCode, string message)
        {
            base.OnCreateRoomFailed(returnCode, message);
            Debug.Log("Filed to Create Room");
        }
        public override void OnJoinRoomFailed(short returnCode, string message)
        {
            base.OnJoinRoomFailed(returnCode, message);
            Debug.Log("Failed to Join Room");
        }


        //[PunRPC]
        //private void RPC_CreatePlayer()
        //{
        //    float randomValue = Random.Range(0f, 5f);
        //    GameObject obj = PhotonNetwork.Instantiate(Path.Combine("Resources", PlayerPrefabName), Vector3.up * randomValue, Quaternion.identity, 0);
        //    //CurrentPlayer = obj.GetComponent<PlayerMovement>();
        //}

        //private void InstantiateCharacter()
        //{
        //            Vector3 spawnPoint = Random.insideUnitSphere.normalized;
        //            spawnPoint.y = 0;
        //            GameObject player = PhotonNetwork.Instantiate(PlayerPrefab.name, spawnPoint, Quaternion.identity, 0);
        //            player.transform.SetParent(this.gameObject.transform);
        //}

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            base.OnPlayerEnteredRoom(newPlayer);
            Debug.Log("Player " + newPlayer.NickName + " Entered Room");
            PhotonNetwork.NickName = this.NickName;
            UI_ConnectionStatus.text = newPlayer.NickName.ToString() + " Joined Room: | " + " Total Players:" + PhotonNetwork.CurrentRoom.PlayerCount;
            PopulatePlayersList();
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {

            base.OnPlayerLeftRoom(otherPlayer);
            UI_ConnectionStatus.text = otherPlayer.NickName.ToString() + " Left Room: | " + " Total Players Remaining: " + ((PhotonNetwork.CurrentRoom.PlayerCount > 1) ? PhotonNetwork.CurrentRoom.PlayerCount.ToString() : "" + "Just you!");
            PopulatePlayersList();
        }

        private void PopulatePlayersList()
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount > 1)
            {
                UI_ConnectionStatus.text += "\r\nCurrent Players in Room:";
                foreach (Player p in PhotonNetwork.PlayerList)
                    UI_ConnectionStatus.text += "\r\n" + p.NickName.ToString();
            }
        }

        public override void OnPlayerPropertiesUpdate(Photon.Realtime.Player target, ExitGames.Client.Photon.Hashtable changedProps)
        {
            //foreach (var change in changedProps)
            //Debug.Log("Property " + change.Key + " of player " + target.UserId + " changed to " + change.Value);
        }

    }
}