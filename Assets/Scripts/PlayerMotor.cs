﻿using Photon.Pun;
using UnityEngine;

namespace SillhouetteGames
{
    [RequireComponent(typeof(PhotonView))]
    [RequireComponent(typeof(PlayerInput))]
    public class PlayerMotor : MonoBehaviourPun, IPunObservable
    {

        [SerializeField]private float forwardMoveSpeed = 7f;
        [SerializeField]private float backwardMoveSpeed = 3.5f;
        [SerializeField]private float turnSpeed = 180f;

        public bool UseTransformView { get; private set; }
        public float Health;

        private PlayerInput playerInput;
        private CharacterController characterController;
        private Vector3 targetPosition;
        private Quaternion targetRotation;

        private Animator characterAnimator;
        float moveSpeed = 1f;
        float rotateSpeed = 200f;


        private void Awake()
        {
            if (!photonView.IsMine) 
                Destroy(GetComponent<PlayerInput>());

            characterAnimator = GetComponent<Animator>();
            playerInput = GetComponent<PlayerInput>();
            characterController = GetComponent<CharacterController>();
            Cursor.lockState = CursorLockMode.Locked;

        }

        private void OnEnable()
        {
            GetComponent<PlayerInput>().OnFire += HandleFire;
        }

        private void OnDisable()
        {
            GetComponent<PlayerInput>().OnFire -= HandleFire;
        }

        private void HandleFire()
        {
            Debug.Log("Fire in the hole!");
        }

        private void Update()
        {
            if (photonView.IsMine)
                DriveThisPlayer();
            else
                DriveOtherPlayers();
        }

        private void DriveThisPlayer()
        {
            var movement = new Vector3(playerInput.Horizontal, 0, playerInput.Vertical);
            transform.Rotate(Vector3.up, playerInput.Horizontal * turnSpeed * Time.deltaTime);

            if (playerInput.Vertical != 0)
            {
                float moveSpeedToUse = (playerInput.Vertical > 0) ? forwardMoveSpeed : backwardMoveSpeed;
                characterController.SimpleMove(transform.forward * moveSpeedToUse * playerInput.Vertical);
            }


            transform.position += transform.forward * (playerInput.Vertical * moveSpeed * Time.deltaTime);
            transform.Rotate(new Vector3(0, playerInput.Horizontal * rotateSpeed * Time.deltaTime, 0));

            //animate
            characterAnimator.SetFloat("RunZ", playerInput.Vertical);
            characterAnimator.SetFloat("RunX", playerInput.Horizontal);
            
        }

        private void DriveOtherPlayers()
        {
            transform.position = Vector3.Lerp(transform.position, targetPosition, 0.25f);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 500 * Time.deltaTime);
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                if (!UseTransformView)
                {
                    stream.SendNext(transform.position);
                    stream.SendNext(transform.rotation);
                }
            }
            else //reading
            {
                if (!UseTransformView)
                {
                    targetPosition = (Vector3)stream.ReceiveNext();
                    targetRotation = (Quaternion)stream.ReceiveNext();
                }
            }
        }
    }
}