﻿using Photon.Pun;
using Photon.Realtime;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SillhouetteGames
{
    public class GameManager : MonoBehaviour
    {
        private PlayerMotor localPlayerMotor;
        public static bool isPlayerInScene;

        private void Awake()
        {
            if (!PhotonNetwork.IsConnected)
                SceneManager.LoadScene(0); //go back to main menu
        }

        void OnEnable()
        {
            SceneManager.sceneLoaded += OnSceneLoaded;
        }
        private void OnDisable()
        {
            SceneManager.sceneLoaded -= OnSceneLoaded;
        }

        public void OnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            Debug.Log("GameManager: Scene Loaded");
            if (PhotonNetwork.IsConnected && !isPlayerInScene)
            {
                isPlayerInScene = true;
                InstantiatePlayerAndSetupScene();
            }
        }

        
        private void InstantiatePlayerAndSetupScene()
        {
            string playerPrefabName = NetworkConnectionManager.instance.PlayerPrefabName;
            Debug.Log(playerPrefabName);
            PhotonNetwork.Instantiate(playerPrefabName, Vector3.zero, Quaternion.identity);
            Debug.Log("GameManager: Player Instantiated");
            //if (PhotonNetwork.IsMasterClient)
            //PhotonNetwork.InstantiateSceneObject("Car", new Vector3(0, 1, 20), Quaternion.identity);
        }
    }
}